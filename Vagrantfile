# -*- mode: ruby -*-
# vi: ft=ruby

# file   : Vagrantfile
# purpose: deploy CentOS 7 The Foreman/Puppet study environment with infrastructure.
#
# author : tycho alexander docter
# date   : 2017/11/23
# version: v1.0

$ansible_clients = <<SCRIPT
sudo sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sudo systemctl restart sshd
SCRIPT

$ansible_admin_server = <<SCRIPT
sudo yum install -y ansible bind-utils nmap tcpdump
sudo echo "10.11.12.4 puppet puppet.doctert.com" >> /etc/hosts
sudo echo "10.11.12.5 admin admin.doctert.com" >> /etc/hosts
mkdir ansible
echo "[all:vars]" > ansible/inventory
echo "ansible_ssh_user=vagrant" >> ansible/inventory
echo "ansible_ssh_pass=vagrant" >> ansible/inventory
echo "" >> ansible/inventory
echo "[puppet]" >> ansible/inventory
echo "puppet.doctert.com" >> ansible/inventory
echo "" >> ansible/inventory
echo "[admin]" >> ansible/inventory
echo "admin.doctert.com" >> ansible/inventory
echo "" >> ansible/inventory
sudo chown -R vagrant:vagrant ansible/
SCRIPT

VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    # disable vagrant-vbguest auto update
    if defined? VagrantVbguest
        config.vbguest.auto_update = false
    end

    ENV["LC_ALL"] = "en_US.UTF-8"
    ENV["LANG"] = "en_US.UTF-8"

    # puppet services are dns/dhcp/thepuppet
    config.vm.define "puppet" do |puppet|
        puppet.vm.box = "centos/7"
        puppet.vm.hostname = "puppet.doctert.com"
        puppet.vm.network "private_network", ip: "10.11.12.4",
            auto_config: false

        puppet.vm.provider "virtualbox" do |vbox|
            vbox.customize [
                "modifyvm", :id,
                "--memory", 1024,
                "--cpus", 1,
                "--name", "puppet",
            ]
        end
        puppet.vm.provision "shell", inline: $ansible_clients
    end

    config.vm.define "admin" do |admin|
        admin.vm.box = "centos/7"
        admin.vm.hostname = "admin.doctert.com"
        admin.vm.network "private_network", ip: "10.11.12.5",
            auto_config: false

        admin.vm.provider "virtualbox" do |vbox|
            vbox.customize [
                "modifyvm", :id,
                "--memory", 512,
                "--cpus", 1,
                "--name", "admin",
            ]
        end
        admin.vm.provision "shell", inline: $ansible_admin_server
    end
end
